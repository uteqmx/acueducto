# Acueducto

> El acueducto de Querétaro, es una monumental edificación actualmente de 74 arcos que alcanzan una altura promedio de 28.5 m y una longitud de 1,280 m. Este acueducto es símbolo de la Ciudad de Querétaro y uno de los más grandes de México. [Wikipedia](https://es.wikipedia.org/wiki/Acueducto_de_Quer%C3%A9taro)
